Module: Testaments - Testimonials with Ratings

Project Page
=============
https://www.drupal.org/project/testaments


Description
============
Module provides a facility for posting Testimonials as well as give ratings.

Module also provides a standard block with form for developers.  This form can
be easily used to integrate in order to collect the Testimonials from the users.

A standard view with Testimonials listing is provided.  One can use this to
either modify or create a new one with its referrence.

Default standard permissions are set and handeled.

Ratings facility is provided so that user can vote the stars.  Ratings is 
considered in 5 Star fashion.

Option to disable the Five Star voting feature is provided for the user's
flexibility.  With this option, one can decide whether h/she wants to use
the rating facility or use simple testimonials functionality.

Rules are used to fire an email to users submitting guestbook entries as well
as to administrator about testimonial submissions.  This helps keeping users
as well as administrators up to date.

Configuration for From Email address, Subject and Body has been provided along
with Enable / Disable Rating feature in admin section to have full control
over this.

Administrator can not only set the Notification settings, but can also decide
the color of Stars to be used suitable to theme of site. Flexibility for
Administrator is given so as to decide whether the Feature should be Enabled but
not Required for users to give votes.

reCAPTCHA module is used to control the spamming.
@see https://www.drupal.org/project/recaptcha
Make sure, FORM_ID is added under Form Protection section of Captcha in order to
use this module.  e.g. 'testimonials_node_form' in this case.

Requirements
============
Module : reCAPTCHA (For Spam control)


Installation
============
1. Copy the module directory in to your Drupal 'modules' directory
and install it as usual.

2. Set / Modify the permissions under 
Home » Administration » People » Node » Testimonials as per your discretion.

3. Simple View with listing testimonials entries has been made available to use.

4. Simple block for testimonials Form has been made available to use.

That's it! You are all set to use the module.


Usage
=====
1. Use Form Block as per your choice.

2. Use View for Listing Entries as per your choice.


Configuration Settings
======================
1. Just ensure the permissions are correctly set for the module in order
to have it working seemlesly.

2. Further configurations of module can be found under 
Home » Administration » Configuration » Testimonials
