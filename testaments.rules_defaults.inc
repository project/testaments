<?php

/**
 * @file
 * Default rule configurations for Testaments.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function testaments_default_rules_configuration() {
  $rules = array();
  $rule = rules_reaction_rule();
  $rule->label = 'Testimonial posting - Acknowledge stakeholders.';
  $rule->active = TRUE;
  $rule->event('testaments_node_acknowledge_user');
  $rule->condition('testaments_user_is_node_owner');
  $rule->action('testaments_notify_user_by_email');
  $configs['rules_testaments_acknowledge_user'] = $rule;
  return $configs;
}
