/**
 * @file
 * Integraton of Ratings behaviors for Testaments.
 */

(function ($) {

  "use strict";

  Drupal.behaviors.testimonials_node_form = {
    attach: function (context, settings) {

      $('span i.fa.fa-star-o').hover(hoverStar, unhoverStar);

      $('span i.fa').click(function () {
        // Get the clicked star id.
        var id = $(this).attr('id');
        // Check if star is validly clicked.
        if (id != '') {
          // First empty the previous value if any.
          $('.field-name-testimonial-rating input').val(0);
          // Unset all the stars.
          for (var i = 5; i > 0; i--) {
            $("span.star-no-" + i + " i#" + i).removeClass('fa-star');
            $("span.star-no-" + i + " i#" + i).addClass('fa-star-o');
          };
          // Now, set the class to selected and previous ones.
          for (var i = id; i > 0; i--) {
            $("span.star-no-" + i + " i#" + i).removeClass('fa-star-o');
            $("span.star-no-" + i + " i#" + i).addClass('fa-star');
          };
          // Finally, assign the selected rating.
          $('.field-name-testimonial-rating input').val(id);
        };
      });
    }
  };

  function unhoverStar() {
    // Get the mouseout id.
    var id = $(this).attr('id');
    if (id == '') {
      id = 5;
    };
    // Get previous ratings if any.
    var rating = $('.field-name-testimonial-rating input').val();

    if (id != '') {
      // Reset the class for all stars.
      for (var i = id; i > 0; i--) {
        $('span.star-no-' + i + ' i#' + i).addClass('fa-star-o');
        $('span.star-no-' + i + ' i#' + i).removeClass('fa-star');
      };
      // Now, set the class to selected and previous ones if set already.
      if (rating != '') {
        for (var i = rating; i > 0; i--) {
          $("span.star-no-" + i + " i#" + i).removeClass('fa-star-o');
          $("span.star-no-" + i + " i#" + i).addClass('fa-star');
        };
      };
    };
  }

  function hoverStar() {
    // Get the hovered id.
    var id = $(this).attr('id');
    if (id != '') {
      // Set the class for hovered and all previous ones.
      for (var i = id; i > 0; i--) {
        $('span.star-no-' + i + ' i#' + i).removeClass('fa-star-o');
        $('span.star-no-' + i + ' i#' + i).addClass('fa-star');
      };
    };
  }

  $(document).ready(function () {
    unhoverStar();
  });
})(jQuery);
