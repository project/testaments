<?php

/**
 * @file
 * Provides the Testaments administration settings.
 */

/**
 * Form callback; administrative settings for testaments.
 */
function testaments_admin_settings() {
  $form['testaments_general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['testaments_general_settings']['testaments_rating_enable'] = array(
    '#type' => 'checkbox',
    '#field_prefix' => t('<strong>Enable</strong> Five Star Rating Feature ?'),
    '#default_value' => variable_get('testaments_rating_enable', FALSE),
  );
  $form['testaments_general_settings']['testaments_rating_status'] = array(
    '#type' => 'checkbox',
    '#field_prefix' => t('Five Star Rating Feature <strong>Required</strong> ?'),
    '#default_value' => variable_get('testaments_rating_status', FALSE),
  );
  $form['testaments_general_settings']['testaments_star_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter Color code or Text for Stars to use'),
    '#default_value' => variable_get('testaments_star_color') ? variable_get('testaments_star_color') : '#000',
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('e.g. #008000 Or green.'),
  );

  $form['testaments_notification_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['testaments_notification_settings']['testaments_email_from'] = array(
    '#type' => 'textfield',
    '#title' => t('Email From Address'),
    '#default_value' => variable_get('testaments_email_from') ? variable_get('testaments_email_from') : variable_get('site_mail'),
    '#maxlength' => 255,
    '#description' => t('From Email Address for sending notifications.'),
    '#required' => TRUE,
  );
  $notification_subject = t('Thank you for your testimonial !');
  $form['testaments_notification_settings']['testaments_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Subject'),
    '#default_value' => variable_get('testaments_email_subject') ? variable_get('testaments_email_subject') : $notification_subject,
    '#maxlength' => 255,
    '#description' => t('Subject for sending notifications.'),
    '#required' => TRUE,
  );
  $notification_body = t('We appreciate your feedback and will get back to you shortly !');
  $form['testaments_notification_settings']['testaments_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Email Body'),
    '#default_value' => variable_get('testaments_email_body') ? variable_get('testaments_email_body') : $notification_body,
    '#cols' => 30,
    '#rows' => 5,
    '#description' => t('Body for sending notifications.'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'testaments_admin_settings_validate';
  $form = system_settings_form($form);
  $form['#submit'][] = 'testaments_admin_settings_set_status';

  return $form;
}

/**
 * Validation function for testaments_admin_settings().
 *
 * @see testaments_admin_settings()
 */
function testaments_admin_settings_validate($form, &$form_state) {
  // Return if standard validations already failed.
  if (form_get_errors()) {
    return;
  }
  // Ensure valid email address is set for From field.
  if ($error = user_validate_mail($form_state['values']['testaments_email_from'])) {
    form_set_error('mail', $error);
  }
}

/**
 * Post Form submission handler for testaments..
 *
 * @see testaments_admin_settings()
 */
function testaments_admin_settings_set_status() {
  // Depending upon rating status set in admin, change the content type field
  // viz., 'testimonial_rating' as either mandatory or optional.
  if (variable_get('testaments_rating_status') !== NULL) {
    // Update field of content type.
    $instances = array(
      // Ratings.
      'testimonial_rating'  => array(
        'field_name'   => 'testimonial_rating',
        'label'        => t('Ratings'),
        'required'     => (variable_get('testaments_rating_status')) ? TRUE : FALSE,
        'settings'       => array(
          'min'  => 1,
          'max'  => 5,
        ),
        'display'       => array(
          'default'  => array(
            'type' => 'hidden',
          ),
          'teaser'  => array(
            'type' => 'hidden',
          ),
        ),

      ),

    );

    // Loop through our instances array.
    foreach ($instances as $instance) {
      $instance['entity_type'] = 'node';
      // Attach the instance to our content type.
      $instance['bundle'] = 'testimonials';
      field_update_instance($instance);
    }
  }
}
