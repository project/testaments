<?php

/**
 * @file
 * Default rule definitions for Testaments.
 */

/**
 * Implements hook_rules_event_info().
 *
 * Rules Event for sending an email to user / admin.
 */
function testaments_rules_event_info() {
  return array(
    'testaments_node_acknowledge_user' => array(
      'group' => t('Testaments'),
      'label' => t('Acknowledge Stakeholders On Testimonial Posting!'),
      'variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Testimonial Node Just Created'),
        ),
        'user' => array(
          'type' => 'user',
          'label' => t('User who created Testimonial'),
        ),
        'target' => array(
          'type' => 'text',
          'label' => t('Whom to Send Email'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 *
 * Rules Action for sending an email to user / admin.
 */
function testaments_rules_action_info() {
  return array(
    'testaments_notify_user_by_email' => array(
      'label' => t('Notify a user/administrator about testimonial submission'),
      'group' => t('Testaments'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => 'User',
        ),
        'node' => array(
          'type' => 'node',
          'label' => 'Target testimonial',
        ),
        'target' => array(
          'type' => 'text',
          'label' => t('Whom to Send Email'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 *
 * Rules Condition for sending an email to user / admin.
 */
function testaments_rules_condition_info() {
  return array(
    'testaments_user_is_node_owner' => array(
      'group' => t('Testaments'),
      'label' => t('User is Testimonial owner'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to send email'),
        ),
        'node' => array(
          'type' => 'node',
          'label' => 'Target testimonial',
        ),
      ),
    ),
  );
}

/**
 * Notify a user / admin for the testimonial post.
 *
 * @param object $user
 *   The user, an author of node.
 * @param object $node
 *   The node, testimonial user has posted.
 * @param string $target
 *   To whom an email is to be fired.
 */
function testaments_notify_user_by_email($user, $node, $target) {
  global $language;
  $module = 'testaments';
  $key = 'testaments_rule';
  $site_language = $language->language;

  if (!is_object($user) || !isset($user->uid)) {
    watchdog('rules_testaments', 'Error: Object passed in "testaments_notify_user_by_email" is not user. Data: !user', array('!user' => print_r($user, TRUE)), WATCHDOG_ERROR);
    return;
  }
  if (!is_object($node) || !isset($node->nid)) {
    watchdog('rules_testaments', 'Error: Object passed in "testaments_notify_user_by_email" is not node. Data: !node', array('!node' => print_r($node, TRUE)), WATCHDOG_ERROR);
    return;
  }

  switch ($target) {
    case 'acknowledge':
      $to = $user->mail;
      $from = variable_get('testaments_email_from') ? variable_get('testaments_email_from') : variable_get('site_mail');
      $notification_subject = t('Thank you for your testimonial !');
      $notification_body = t('We appreciate your feedback and will get back to you shortly !');
      $params = array(
        'subject' => variable_get('testaments_email_subject') ? variable_get('testaments_email_subject') : $notification_subject,
        'message' => variable_get('testaments_email_body') ? variable_get('testaments_email_body') : $notification_body,
      );
      drupal_mail($module, $key, $to, $site_language, $params, $from);
      watchdog('rules_testaments', 'Testament Email Sent To !receiver From !sender', array('!receiver' => $to, '!sender' => $from), WATCHDOG_INFO);
      break;

    case 'inform':
      $to = variable_get('testaments_email_from') ? variable_get('testaments_email_from') : variable_get('site_mail');
      $from = $user->mail;
      $notification_subject = t("New Testimonial posted.");
      $notification_body = t("New Testimonial is posted on site. !url.", array('!url' => l(t('View'), url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)))));
      $params = array(
        'subject' => $notification_subject,
        'message' => $notification_body,
      );
      drupal_mail($module, $key, $to, $site_language, $params, $from);
      watchdog('rules_testaments', 'Testament Email Sent To !receiver From !sender', array('!receiver' => $to, '!sender' => $from), WATCHDOG_INFO);
      break;

    default:
      watchdog('rules_testaments', 'Error: Target for sending email is not clear.', array(), WATCHDOG_ERROR);
      break;
  }
}

/**
 * Check an author of testimonial node..
 *
 * @param object $user
 *   The user, an author of node.
 * @param object $node
 *   The node, testimonial user has posted.
 */
function testaments_user_is_node_owner($user, $node) {
  if (!is_object($user) || !isset($user->uid)) {
    watchdog('rules_testaments', 'Error: Object passed in "testaments_user_is_node_owner" is not user. Data: !user', array('!user' => print_r($user, TRUE)), WATCHDOG_ERROR);
    return;
  }
  if (!is_object($node) || !isset($node->nid)) {
    watchdog('rules_testaments', 'Error: Object passed in "testaments_user_is_node_owner" is not node. Data: !node', array('!node' => print_r($node, TRUE)), WATCHDOG_ERROR);
    return;
  }
  if ($user->uid === $node->uid) {
    return TRUE;
  } else {
    watchdog('rules_testaments', 'Error: user is not author of testament. Data: !user for !node', array('!user' => $user->uid, '!node' => $node->nid), WATCHDOG_ERROR);
    return FALSE;
  } 
}
