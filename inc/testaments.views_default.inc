<?php

/**
 * @file
 * Inc file for testimonials entries view.
 */

/**
 * Implements hook_views_default_views().
 */
function testaments_views_default_views() {
  // Creates a defaults view as a user helper.
  return testaments_view_list();
}

/**
 * Begin view.
 *
 * View for Testimonials users listing.
 */
function testaments_view_list() {
  $view = new view();
  $view->name = 'testimonials_list';
  $view->description = 'List of all Testimonials Entries to the site.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Testimonials List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'testimonial_name' => 'testimonial_name',
    'title' => 'title',
    'nothing' => 'nothing',
    'testimonial_message' => 'testimonial_message',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'testimonial_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'testimonial_message' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Name */
  $handler->display->display_options['fields']['testimonial_name']['id'] = 'testimonial_name';
  $handler->display->display_options['fields']['testimonial_name']['table'] = 'field_data_testimonial_name';
  $handler->display->display_options['fields']['testimonial_name']['field'] = 'testimonial_name';
  $handler->display->display_options['fields']['testimonial_name']['label'] = '';
  $handler->display->display_options['fields']['testimonial_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['testimonial_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['testimonial_name']['type'] = 'text_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[testimonial_name]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Collective Entry */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Collective Entry';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="mailto:[title]">[testimonial_name]</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Content: Testimonial */
  $handler->display->display_options['fields']['testimonial_message']['id'] = 'testimonial_message';
  $handler->display->display_options['fields']['testimonial_message']['table'] = 'field_data_testimonial_message';
  $handler->display->display_options['fields']['testimonial_message']['field'] = 'testimonial_message';
  $handler->display->display_options['fields']['testimonial_message']['alter']['max_length'] = '60';
  $handler->display->display_options['fields']['testimonial_message']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['testimonial_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['testimonial_message']['type'] = 'text_plain';
  $handler->display->display_options['fields']['testimonial_message']['settings'] = array(
    'trim_length' => '60',
  );
  /* Field: Content: Ratings */
  $handler->display->display_options['fields']['testimonial_rating']['id'] = 'testimonial_rating';
  $handler->display->display_options['fields']['testimonial_rating']['table'] = 'field_data_testimonial_rating';
  $handler->display->display_options['fields']['testimonial_rating']['field'] = 'testimonial_rating';
  $handler->display->display_options['fields']['testimonial_rating']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['testimonial_rating']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['testimonial_rating']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );

  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'testimonials' => 'testimonials',
  );

  /* Display: Testimonials List */
  $handler = $view->new_display('block', 'Testimonials List', 'block_1');
  $handler->display->display_options['block_description'] = 'Testimonials List';

  $views[$view->name] = $view;
  return $views;
}
